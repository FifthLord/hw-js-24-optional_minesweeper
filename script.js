
let allCell;
let mainDiv
let mines;
let wrapArr = [];
let minesCount = 0;
let minesCountTrue = 0;
let countMined;
let tableWidth = 8;
let tableHight = 8;

function createTable() {
   do {
      tableWidth = +prompt('Введіть ширину поля', "Не менше 8");
   } while (tableWidth < 8 || tableWidth > 201 || isNaN(tableWidth));
   do {
      tableHight = +prompt('Введіть висоту поля', "Не менше 8");
   } while (tableHight < 8 || tableHight > 201 || isNaN(tableHight));

   document.body.insertAdjacentHTML('beforeend', `<div class="main-div-rect"></div>`);
   mainDiv = document.querySelector('.main-div-rect');
   mainDiv.addEventListener('contextmenu', toggleFlag);
   mainDiv.addEventListener('click', checkCell);
   mainDiv.addEventListener('dblclick', openNeighboringCells);

   for (let i = 0; i < tableHight; i++) {
      mainDiv.insertAdjacentHTML('beforeend', `<div id='row ${i}' class='div-row'></div>`)
   }
   Array.from(mainDiv.children).forEach(elem => {
      for (let j = 0; j < tableWidth; j++) {
         elem.insertAdjacentHTML('beforeend', `<div data-number = '0' data-mined = 'false' class='div-item'></div>`);
      }
   });

   //--------створюємо міни------//
   allCell = document.querySelectorAll('.div-item');
   generateMine(allCell);

   //--------додаємо числа------//
   for (let i = 0; i < tableHight; i++) {
      let wrap = document.getElementById(`row ${i}`)
      wrapArr.push(wrap.children)
   };
   generateNum();

   //--------додаємо рахунок------//
   mainDiv.insertAdjacentHTML('beforebegin', `<div class="count-mined">Відкрито мін ${minesCount}/${mines}</div>`);
   countMined = document.querySelector('.count-mined');

   //--------додаємо кнопку перезапуску------//
   mainDiv.insertAdjacentHTML('afterend', `<button class="btn" onClick = 'startNewGame()'>Розпочати нову гру</button>`);
};
createTable()

//--------створюємо міни------//
function generateMine(array) {
   mines = Math.floor(array.length / 6);
   for (let i = 0; i < mines; i++) {
      let randomNum = Math.floor(Math.random() * array.length);
      if (array[randomNum].dataset.mined !== 'true') {
         array[randomNum].dataset.mined = 'true';
      } else {
         i--
      }
   }
};

//--------задаємо числа в комірках------//
function generateNum() {
   for (let row = 0; row < tableHight; row++) {
      for (let col = 0; col < tableWidth; col++) {
         if (wrapArr[row][col].dataset.mined === 'true') {
            if (row - 1 >= 0 && col - 1 >= 0) {
               wrapArr[row - 1][col - 1].dataset.number = +wrapArr[row - 1][col - 1].dataset.number + 1;
            }
            if (row - 1 >= 0) {
               wrapArr[row - 1][col].dataset.number = +wrapArr[row - 1][col].dataset.number + 1;
            }
            if (row - 1 >= 0 && [col + 1] in wrapArr[row - 1]) {
               wrapArr[row - 1][col + 1].dataset.number = +wrapArr[row - 1][col + 1].dataset.number + 1;
            }
            if ([col + 1] in wrapArr[row]) {
               wrapArr[row][col + 1].dataset.number = +wrapArr[row][col + 1].dataset.number + 1;
            }
            if (col - 1 >= 0 && [col - 1] in wrapArr[row]) {
               wrapArr[row][col - 1].dataset.number = +wrapArr[row][col - 1].dataset.number + 1;
            }
            if ([row + 1] in wrapArr && col - 1 >= 0 && [col - 1] in wrapArr[row + 1]) {
               wrapArr[row + 1][col - 1].dataset.number = +wrapArr[row + 1][col - 1].dataset.number + 1;
            }
            if ([row + 1] in wrapArr) {
               wrapArr[row + 1][col].dataset.number = +wrapArr[row + 1][col].dataset.number + 1;
            }
            if ([row + 1] in wrapArr && [col + 1] in wrapArr[row + 1]) {
               wrapArr[row + 1][col + 1].dataset.number = +wrapArr[row + 1][col + 1].dataset.number + 1;
            }
         }
      }
   }
};

//--------ставимо флаг при ПКМ------//
function toggleFlag(event) {
   event.preventDefault();
   if (event.target.closest('.div-item') && !event.target.classList.contains('checked')) {
      if (event.target.classList.contains('flag')) {
         event.target.classList.remove('flag');
         minesCount--;
         if (event.target.dataset.mined === 'true') {
            minesCountTrue--
         }
      } else {
         event.target.classList.add('flag');
         minesCount++;
         if (event.target.dataset.mined === 'true') {
            minesCountTrue++
         }
      }
      countMined.innerHTML = `Відкрито мін ${minesCount}/${mines}`
   }
   if (minesCount === mines && minesCount === minesCountTrue) {
      countMined.innerHTML = `Ти Виграв!`
      mainDiv.removeEventListener('contextmenu', toggleFlag);
      mainDiv.removeEventListener('click', checkCell);
   }
};

//--------при ЛКМ------//
function checkCell(event) {
   event.preventDefault();
   let num = +event.target.dataset.number;
   if (event.target.classList.contains('flag')) return false;

   //----в разі програшу---//
   if (event.target.dataset.mined === 'true') {
      mainDiv.removeEventListener('contextmenu', toggleFlag);
      mainDiv.removeEventListener('click', checkCell);
      countMined.innerHTML = `Ви програли!`;
      let head = document.getElementsByTagName('head')[0];
      head.insertAdjacentHTML('beforeend', `
      <style>
      .div-item[data-mined = "true"]{
         background-color: red;
      </style>
      `);
      //----логіка вскриття комірок---//
   } else if (event.target.closest('.div-item') && !event.target.classList.contains('checked')) {
      event.target.classList.add('checked');
      if (num !== 0) {
         event.target.innerHTML = num
      } else {
         let position = Array.from(allCell).indexOf(event.target);
         let row = Math.floor(position / tableHight);
         let col = position % tableWidth;

         if (row - 1 >= 0 && col - 1 >= 0) {
            wrapArr[row - 1][col - 1].click()
         }
         if (row - 1 >= 0) {
            wrapArr[row - 1][col].click()
         }
         if (row - 1 >= 0 && [col + 1] in wrapArr[row - 1]) {
            wrapArr[row - 1][col + 1].click()
         }
         if ([col + 1] in wrapArr[row]) {
            wrapArr[row][col + 1].click()
         }
         if (col - 1 >= 0 && [col - 1] in wrapArr[row]) {
            wrapArr[row][col - 1].click()
         }
         if ([row + 1] in wrapArr && col - 1 >= 0 && [col - 1] in wrapArr[row + 1]) {
            wrapArr[row + 1][col - 1].click()
         }
         if ([row + 1] in wrapArr) {
            wrapArr[row + 1][col].click()
         }
         if ([row + 1] in wrapArr && [col + 1] in wrapArr[row + 1]) {
            wrapArr[row + 1][col + 1].click()
         }
      }
   }
};

//----при подвійному кліку---//
function openNeighboringCells(e) {
   if (e.target.closest('.div-item') && e.target.classList.contains('checked')) {
      let flagCount = 0;
      let position = Array.from(allCell).indexOf(e.target);
      let row = Math.floor(position / tableHight);
      let col = position % tableWidth;

      if (row - 1 >= 0 && col - 1 >= 0) {
         if (wrapArr[row - 1][col - 1].classList.contains('flag')) flagCount++
      }
      if (row - 1 >= 0) {
         if (wrapArr[row - 1][col].classList.contains('flag')) flagCount++
      }
      if (row - 1 >= 0 && [col + 1] in wrapArr[row - 1]) {
         if (wrapArr[row - 1][col + 1].classList.contains('flag')) flagCount++
      }
      if ([col + 1] in wrapArr[row]) {
         if (wrapArr[row][col + 1].classList.contains('flag')) flagCount++
      }
      if (col - 1 >= 0 && [col - 1] in wrapArr[row]) {
         if (wrapArr[row][col - 1].classList.contains('flag')) flagCount++
      }
      if ([row + 1] in wrapArr && col - 1 >= 0 && [col - 1] in wrapArr[row + 1]) {
         if (wrapArr[row + 1][col - 1].classList.contains('flag')) flagCount++
      }
      if ([row + 1] in wrapArr) {
         if (wrapArr[row + 1][col].classList.contains('flag')) flagCount++
      }
      if ([row + 1] in wrapArr && [col + 1] in wrapArr[row + 1]) {
         if (wrapArr[row + 1][col + 1].classList.contains('flag')) flagCount++
      }
      if (flagCount === +e.target.dataset.number) {
         if (row - 1 >= 0 && col - 1 >= 0) {
            wrapArr[row - 1][col - 1].click()
         }
         if (row - 1 >= 0) {
            wrapArr[row - 1][col].click()
         }
         if (row - 1 >= 0 && [col + 1] in wrapArr[row - 1]) {
            wrapArr[row - 1][col + 1].click()
         }
         if ([col + 1] in wrapArr[row]) {
            wrapArr[row][col + 1].click()
         }
         if (col - 1 >= 0 && [col - 1] in wrapArr[row]) {
            wrapArr[row][col - 1].click()
         }
         if ([row + 1] in wrapArr && col - 1 >= 0 && [col - 1] in wrapArr[row + 1]) {
            wrapArr[row + 1][col - 1].click()
         }
         if ([row + 1] in wrapArr) {
            wrapArr[row + 1][col].click()
         }
         if ([row + 1] in wrapArr && [col + 1] in wrapArr[row + 1]) {
            wrapArr[row + 1][col + 1].click()
         }
      }
   }
}

//----логіка кнопки нової гри---//
function startNewGame() {
   document.body.innerHTML = "";
   if (document.getElementsByTagName('style')[0]) {
      document.getElementsByTagName('style')[0].remove();
   }
   tableWidth = 8;
   tableHight = 8;
   wrapArr = [];
   minesCount = 0;
   minesCountTrue = 0;
   createTable();
};